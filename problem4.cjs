const inventory = require('./cars.cjs')

// ARROW FUNCTION
// const output = inventory.map((x) => x.car_year)

const output = inventory.map(function abcd(x){
    return x.car_year
})

module.exports = output;