const inventory = require('./cars.cjs')

// ARROW FUNCTION
// const output = inventory.filter((x) => x.car_make == 'BMW' || x.car_make == 'Audi')

const output = inventory.filter(function abcd(x){
    if(x.car_make == 'BMW' || x.car_make == 'Audi'){
        return x.car_make
    }
})

module.exports = output;