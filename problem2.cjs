const inventory = require('./cars.cjs')

/* ARROW FUNCTION */
// const output = inventory.filter((x) => x.id == inventory.length)

const output= inventory.filter(function (x) {
    if(x.id == inventory.length){
        return x
    }
})

module.exports = output;