const inventory = require('./cars.cjs');

let arr = [];

/* ARROW FUNCTION */
// const output = inventory.map((x) => x.car_make)

const output = inventory.map(function abcd(x){
    
    return x.car_make
})

module.exports = output;
